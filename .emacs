(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (adwaita)))
 '(org-html-head
   "<link rel=\"stylesheet\" href=\"https://anno.bitbucket.io/org-style.css\">")
 '(org-export-with-section-numbers nil)
 '(org-export-with-sub-superscripts (quote {}))
 '(org-startup-folded nil)
 '(org-use-sub-superscripts (quote {}))
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("org" . "http://orgmode.org/elpa/")
     ("melpa" . "https://stable.melpa.org/packages/"))))
 '(package-selected-packages (quote (web-mode org-plus-contrib org)))
 '(standard-indent 2)
 '(tab-always-indent t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(package-initialize)

(setenv "EDITOR" "emacsclient")
(setenv "PAGER" "cat")

(defun my-revert-buffer ()
  (interactive)
  (revert-buffer t t))

(setq text-mode-hook 
      '(lambda (&rest ignore)
	 (auto-fill-mode 1)			
	 (set-fill-column 75)))

(defun shell-current-directory ()
  (interactive)
  (let ((dir default-directory))
    (shell)
    (process-send-string (get-buffer-process (current-buffer))
			 (format "cd \"%s\"\n" (replace-regexp-in-string "~" (getenv "HOME") dir)))
    (setq default-directory dir))) 

(setq shell-mode-hook
      '(lambda (&rest ignore)
	 (global-set-key "s" 'shell-current-directory)
	 (local-set-key (kbd "M-p") 'comint-previous-matching-input-from-input)))

(add-hook 'dired-mode-hook '(lambda () (local-set-key (kbd "M-s") 'shell-current-directory)))

(defun find-uses ()
   (interactive)
  (save-excursion
    (backward-word 1)
    (and (looking-at "[A-Za-z][A-Za-z0-9_$]*")
	 (occur (concat "\\<" (match-string 0) "\\>")))))

(global-set-key "u" 'find-uses)
(global-set-key "c" 'compile)
(global-set-key "g" 'my-revert-buffer)
(global-set-key "s" 'shell)
(global-set-key "f" 'find-file-at-point)
(global-set-key "n" 'goto-line)

(put 'eval-expression 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(add-to-list 'auto-mode-alist '("\\.html$" . web-mode))
(require 'clang-format)
(add-hook 'before-save-hook
	  '(lambda () (if (string-match-p "[.]\\(cc\\|h\\)\\'" buffer-file-name)
			  (clang-format-buffer))))
(ido-mode)
(cua-mode)
(server-start)
